#!/bin/bash

# Open up required ports
sudo ufw allow 16126/tcp
sudo ufw allow 16127/tcp
sudo ufw allow 16128/tcp
sudo ufw allow 27017/tcp      # default mongodb port

# Install Mongodb, enable, and start service
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt-get update
sudo apt-get install mongodb-org -y
sudo service mongod start

# Install Nodejs
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install nodejs -y

# Clone Zelnoded repo and start ZelFlux
git clone https://dk808@bitbucket.org/dk808/zelflux.git && cd zelflux
npm start

